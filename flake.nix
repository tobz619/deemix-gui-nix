{
  description = "A nix flake for deemix-gui";
  
  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-23.11;
  inputs.flake-utils.url = github:numtide/flake-utils;

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        node-modules = pkgs.fetchYarnDeps {
          name = "node-modules";
          src = ./.;
        };
        deemix-gui = pkgs.stdenv.mkDerivation {
          name = "deemix-gui";
          src = pkgs.fetchgit {
            url = "https://gitlab.com/RemixDev/deemix-gui.git";
            rev = "5d447b6035d9771757db9340da87a0375606971f"; 
            sha256 = "15pfz2n9wgg6ygxp4gmqm0zk87hx4143zvihqcgv5sj440wnq75w";
          };
          buildInputs = with pkgs; [ yarn node-modules ];
          buildPhase = ''
            ln -s ${node-modules}/libexec/yarn-nix-example/node_modules node_modules
            ${pkgs.yarn}/bin/yarn build
          '';
          installPhase = ''
            mkdir $out
            mv dist $out/lib
          '';

        };
      in
       {
          packages = {
            inherit node-modules;
            default = deemix-gui;
          };
        }
  );
}

